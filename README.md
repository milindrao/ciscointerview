# README #

Java Challenge Interview for CISCO Position

* Intentionally left out most error checking to make the code more readable for the purpose of the interview
* Only happy path test cases to show the code working
* I'm sure there are more efficient and elegant solutions to getting the paths of a tree. But I believe it works.  But I haven't tested all the corner cases