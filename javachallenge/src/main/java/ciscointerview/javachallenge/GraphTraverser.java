package ciscointerview.javachallenge;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Class to traverse through a graph. 
 * 
 */
public class GraphTraverser
{
	/**
	 * Returns a list of paths (each path is a list of nodes) from the root
	 * to each leaf ofthe tree. 
	 * 
	 * @param theNode the node to start with
	 * @return a list of a list of nodes 
	 */
	public ArrayList<ArrayList<GNode>> paths(GNode theNode)
    {
    	ArrayList<ArrayList<GNode>> thePaths = new ArrayList<ArrayList<GNode>>();
    	
    	Set<GNode> theVisitedNodes = new HashSet<GNode>();
    	
    	// Walk through the tree and get all the paths from the root to the leaf
    	// node.
    	while (true) {
    		// Create a path array and pass it to the setPath method to get
    		// populated
    		ArrayList<GNode> thePath = new ArrayList<GNode>();
	    	setPath(theNode, thePath, theVisitedNodes);
	    	if (!thePath.isEmpty()) {
	    		// Add the path to the list of paths and continue with the next one
	    		thePaths.add(thePath);
	    		if (theNode.getChildren().length == 0) {
	    			// The node is a leaf node
	    			break;
	    		}
	    	} else {
	    		// No more paths remaining.  Return
	    		break;
	    	}
    	};
    	
    	return thePaths;
    }
    
    
	/**
	 * Recursively call this method to set the path from the node all the way
	 * to the leaf.
	 *  
	 * @param theNode the current node
	 * @param thePath the path that is updated through recursive calls
	 * @param theVisitedNodes the nodes that have been visited as we traverse
	 * the graph
	 */
    private void setPath(GNode theNode, 
    					 ArrayList<GNode> thePath, 
    					 Set<GNode> theVisitedNodes)
	{
    	int numChildren = theNode.getChildren().length; 
    	
    	if (numChildren == 0) {
    	
    		// Leaf Node.  Add it to the path, mark the node as visited and
    		// return 
    		thePath.add(theNode);
    		theVisitedNodes.add(theNode);

    	} else {
    		
    		for (int i = 0; i < numChildren; i++) {
    			GNode currGNode = theNode.getChildren()[i];
    			// Only visit the children that haven't been visited yet
    			if (!theVisitedNodes.contains(currGNode)) {
    				// Add the current node to the path on the way down
    				thePath.add(theNode);
    				// Call the method recursively now on the child node
					setPath(currGNode, thePath, theVisitedNodes);
	    			if (i == numChildren-1) {
	    				// All the children have been visited.  Mark this
	    				// node as visited as well.
	    				theVisitedNodes.add(theNode);
	    			} else {
	    				// We have gone down one path.  Return up the stack to root
						break;
	    			}
    			}
			}
    	}
	}


	/**
	 * This method walks the graph starting with the specified node and down
	 * the hierarchy.  Does not show any ancestors.
	 *  
	 * @param theNode the node from which the graph is to be traversed 
	 * @return a list of all the descendants of the specified node depth first 
	 * left to right.
	 */
    public ArrayList<GNode> walkGraph(GNode theNode)
    {
    	ArrayList<GNode> theGraph = new ArrayList<>();
    	
    	theGraph.add(theNode);
    	
    	for (GNode currGNode : theNode.getChildren()) {
			theGraph.addAll(walkGraph(currGNode));
		}
    	
    	return theGraph;
    }
}
