package ciscointerview.javachallenge;

public interface GNode 
{
    public String getName();
    public GNode[] getChildren();
}
