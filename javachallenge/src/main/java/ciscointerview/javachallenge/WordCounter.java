package ciscointerview.javachallenge;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Map;
import java.util.TreeMap;

/**
 * Counts the words read from a file
 * @author milind
 *
 */
public class WordCounter
{
	public static void main(String[] args)
	{
		if (args.length == 1) {
			try {
				File theFile = new File(args[0]);
				WordCounter theApp = new WordCounter();
				Map<String, Integer> theWordCountMap = theApp.countWords(theFile);
				for (String currWord : theWordCountMap.keySet()) {
					System.out.println(currWord + ": " + theWordCountMap.get(currWord));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Usage: java Wordcounter <filename>");
			System.exit (-1);
		}
	}

	
	/**
	 * Read all the words in the specified file count the number of occurrences  
	 * of each word in the file  
	 */
	Map<String, Integer> countWords(File theFile)
		throws Exception
	{
		Map<String, Integer> theWordCountMap;
		
		if ((theFile != null) && theFile.exists() && theFile.isFile()) {
			
			// For real production, we would be stream reading the file, but
			// for this purpose just read the entire file into a string.
			String theContent = new String (Files.readAllBytes(theFile.toPath()),
										    Charset.forName("UTF-8"));
			
			theWordCountMap = countWords(theContent);
			
		} else {
			String msg = "'" + theFile + "' is not a valid file";
			throw new Exception(msg);
		}
		
		return theWordCountMap;
	}


	/**
	 * Tokenize the specified string and count the number of occurrences of 
	 * each word in the string  
	 */
	Map<String, Integer> countWords(String theContent)
	{
		Map<String, Integer> theWordCountMap = new TreeMap<>(); 
		
		String[] words = theContent.split("\\W+");
		for (String currWord : words) {
			String currKey = currWord.toLowerCase();
			if (!currKey.isEmpty()) {
				Integer currCount = theWordCountMap.get(currKey);
				if (currCount == null) {
					theWordCountMap.put(currKey, 1);
				} else {
					theWordCountMap.put(currKey, ++currCount);
				}
			}
		}
		
		return theWordCountMap;
	}
}
