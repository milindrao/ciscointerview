package ciscointerview.javachallenge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GNodeImpl implements GNode
{
	private String iName;
	private List<GNode> iChildren;
	
	
	public GNodeImpl(String theName)
	{
		this(theName, null);
	}

	public GNodeImpl(String theName, GNode[] theChildren)
	{
		iName = theName;
		iChildren = new ArrayList<>();
		addChildren(theChildren);
	}
	
	public void addChild(GNode theChild)
	{
		if (theChild != null) {
			iChildren.add(theChild);
		}
	}
	
	public void addChildren(GNode[] theChildren)
	{
		if (theChildren != null) {
			iChildren = new ArrayList<>(Arrays.asList(theChildren));
		}
	}
	
	public GNode[] getChildren()
	{
		return iChildren.toArray(new GNode[iChildren.size()]);
	}

	public String getName()
	{
		return iName;
	}
	
	@Override
	public String toString()
	{
		return iName;
	}
}
