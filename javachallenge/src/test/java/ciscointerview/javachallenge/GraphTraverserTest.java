package ciscointerview.javachallenge;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

/**
 * Happy Path unit tests only.  No error checking.
 *  
 */
public class GraphTraverserTest
{
	/**
	 * Test the retrieval of all the paths of the following tree 
	 * 
	 *                         A
	 *                 B           C         D
	 *               E   F     G   H  I      J 
	 *   
	 */
	@Test
	public void testPaths()
		throws Exception
	{
		GNode[] nodes = new GNode[10];
		for (int i = 0; i < 10; i++) {
			int nodeName = (int)'A' + i;
			nodes[i] = new GNodeImpl(Character.toString((char) nodeName));
		}
		// Add B, C, D to A
		((GNodeImpl)nodes[0]).addChildren(new GNode[] {nodes[1], nodes[2], nodes[3]});
		// Add E, F to B
		((GNodeImpl)nodes[1]).addChildren(new GNode[] {nodes[4], nodes[5]});
		// Add G, H, I to C
		((GNodeImpl)nodes[2]).addChildren(new GNode[] {nodes[6], nodes[7], nodes[8]});
		// Add J to D
		((GNodeImpl)nodes[3]).addChild(nodes[9]);

		ArrayList<ArrayList<GNode>> thePaths = new GraphTraverser().paths(nodes[0]);
		Assert.assertEquals("[[A, B, E], [A, B, F], [A, C, G], [A, C, H], [A, C, I], [A, D, J]]", thePaths.toString());
	}
	
	
	/**
	 * Test the retrieval of all the paths of a tree with a single path 
	 * 
	 *          root
	 *            1
	 *            2
	 *            3
	 *            4
	 */
	@Test
	public void testPathsForSingleBranch()
		throws Exception
	{
		GNode root = new GNodeImpl("Root");
		GNode level1 = new GNodeImpl("1");
		((GNodeImpl)root).addChild(level1);
		GNode level2 = new GNodeImpl("2");
		((GNodeImpl)level1).addChild(level2);
		GNode level3 = new GNodeImpl("3");
		((GNodeImpl)level2).addChild(level3);
		GNode level4 = new GNodeImpl("4");
		((GNodeImpl)level3).addChild(level4);
		ArrayList<ArrayList<GNode>> thePaths = new GraphTraverser().paths(root);
		Assert.assertEquals("[[Root, 1, 2, 3, 4]]", thePaths.toString());
	}
	
	
	/**
	 * Test the retrieval of all the paths of a tree with a solitary node. 
	 */
	@Test
	public void testPathsForSingleNode()
		throws Exception
	{
		GNode root = new GNodeImpl("Root");
		ArrayList<ArrayList<GNode>> thePaths = new GraphTraverser().paths(root);
		Assert.assertEquals("[[Root]]", thePaths.toString());
	}
	
	
	
	/**
	 * Test a linear graph walk 
	 * 
	 *          root
	 *            1
	 *            2
	 *            3
	 */
	@Test
	public void testWalkGraphWithSingleBranch()
		throws Exception
	{
		GNode root = new GNodeImpl("Root");
		GNode level1 = new GNodeImpl("1");
		((GNodeImpl)root).addChild(level1);
		GNode level2 = new GNodeImpl("2");
		((GNodeImpl)level1).addChild(level2);
		GNode level3 = new GNodeImpl("3");
		((GNodeImpl)level2).addChild(level3);

		List<GNode> theGraph = new GraphTraverser().walkGraph(root);
		Assert.assertEquals("[Root, 1, 2, 3]", theGraph.toString());
	}
	
	
	/**
	 * Test walking the graph of a solitary node.
	 */
	@Test
	public void testWalkGraphWithSingleNode()
		throws Exception
	{
		GNode root = new GNodeImpl("Root");
		List<GNode> theGraph = new GraphTraverser().walkGraph(root);
		Assert.assertEquals("[Root]", theGraph.toString());
	}
	
	
	/**
	 * Test 3 levels of graph with decreasing number of leaves per child at 
	 * each level.
	 * 
	 *                         root
	 *                 1           2           3
	 *               4   5       6   7       8   9
	 *              10   11     12  13      14   15
	 *   
	 */
	@Test
	public void testWalkGraphWithThreeLevels()
		throws Exception
	{
		GNode root = new GNodeImpl("Root");
		
		List<GNode> level1 = new ArrayList<>();
		Integer count = 1;
		int numChildren = 3;
		for (int i = 0; i < numChildren; i++, count++) {
			GNode currNode = new GNodeImpl(count.toString());
			level1.add(currNode);
			((GNodeImpl)root).addChild(currNode);
		}

		List<GNode> level2 = new ArrayList<>();
		for (GNode currLevel1Node : level1) {
			numChildren = 2;
			for (int i = 0; i < numChildren; i++, count++) {
				GNode currNode = new GNodeImpl(count.toString());
				level2.add(currNode);
				((GNodeImpl)currLevel1Node).addChild(currNode);
			}
		}

		List<GNode> level3 = new ArrayList<>();
		for (GNode currLevel2Node : level2) {
			numChildren = 1;
			for (int i = 0; i < numChildren; i++, count++) {
				GNode currNode = new GNodeImpl(count.toString());
				level3.add(currNode);
				((GNodeImpl)currLevel2Node).addChild(currNode);
			}
		}
		
		List<GNode> theGraph = new GraphTraverser().walkGraph(root);
		Assert.assertEquals("[Root, 1, 4, 10, 5, 11, 2, 6, 12, 7, 13, 3, 8, 14, 9, 15]", theGraph.toString());
	}
}
