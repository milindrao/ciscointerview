package ciscointerview.javachallenge;

import java.io.File;
import java.nio.file.Files;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

/**
 * Happy Path testing of the WordCounter class only.  No error checking.
 */
public class WordCounterTest
{
	/**
	 * Test the counting of words in a file
	 */
	@Test
	public void testCountWordsInFile()
		throws Exception
	{
		// Create the file
		String theContent = 
			"\n" +
			"The Brown Fox Quickly jumped over the log \n" +
			"(   )\n" + 
			"Put simply, I got tired of simply writing \"cascading\" --help files\n" + 
			"I quickly ate the brown 8 log since I was tired";
		
		File theFile = File.createTempFile("WordCounter", ".tmp"); 
		theFile.deleteOnExit();
		Files.write(theFile.toPath(), theContent.getBytes());

		String theExpectedResult = "{8=1, ate=1, brown=2, cascading=1, files=1, fox=1, got=1, help=1, i=3, jumped=1, log=2, of=1, over=1, put=1, quickly=2, simply=2, since=1, the=3, tired=2, was=1, writing=1}"; 
		
		WordCounter theWordCounter = new WordCounter();

		Map<String, Integer> currWCMap = theWordCounter.countWords(theFile);
		Assert.assertEquals(theExpectedResult, currWCMap.toString());
	}
	
	
	/**
	 * Test the counting of words in a string
	 */
	@Test
	public void testCountWordsInString()
		throws Exception
	{
		String[] theInput = new String[]
		{
			"",
			"The Quick Brown Fox jumped over the log",
			"Put simply, I got tired of simply writing \"cascading\" --help files",
			"a the a at the at a the the it",
		};

		String[] theExpectedResults = new String[]
		{
			"{}",
			"{brown=1, fox=1, jumped=1, log=1, over=1, quick=1, the=2}",		
			"{cascading=1, files=1, got=1, help=1, i=1, of=1, put=1, simply=2, tired=1, writing=1}",
			"{a=3, at=2, it=1, the=4}",
		};
		
		WordCounter theWordCounter = new WordCounter();
		
		for (int i = 0; i < theInput.length; i++) {
			String currContent = theInput[i];
			Map<String, Integer> currWCMap = theWordCounter.countWords(currContent);
			Assert.assertEquals(theExpectedResults[i], currWCMap.toString());
		}
	}
	
}

